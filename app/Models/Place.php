<?php

namespace App\Models;

class Place extends BaseModel
{
    protected $table = 'places';

    protected $fillable = [
        'title_ru',
        'title_ky',
        'position',
    ];

    protected $guarded = [
        'id',
        'added_by',
        'updated_at',
        'created_at',
        'parent_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function parent() {
        return $this->hasOne('App\Models\Place', 'id', 'parent_id');
    }
}
