<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Laravel\Lumen\Auth\Authorizable;

class User extends BaseModel implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'users';
    protected $dates = ['created_at', 'updated_at', 'last_action'];

    protected $fillable = [
        'phone_number',
        'gender',
        'full_name',
        'contacts',
        'picture',
        'bio',
        'language',
    ];

    protected $casts = [
        'abilities' => 'array',
        'contacts' => 'array',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function hasAbility($a = 'admin') {
        return in_array($a, $this->abilities) || in_array('admin', $this->abilities);
    }
}
