<?php

namespace App\Models;

class Vehicle extends BaseModel
{
    protected $table = 'vehicles';

    protected $fillable = [
        'title',
        'state_number',
        'picture',
        'data_sheet_front_picture',
        'data_sheet_back_picture',
        'data_sheet_expiration_date',
        'type',
        'persons',
    ];

    protected $guarded = [
        'id',
        'user_id',
        'updated_at',
        'created_at',
        'confirmed_by',
        'confirmed_at',
    ];

    protected $hidden = [];

    public function scopeConfirmed($query) {
        return $query->whereNotNull('confirmed_by')->whereNotNull('confirmed_at');
    }
}
