<?php

namespace App\Models;

class Driver extends BaseModel
{
    protected $table = 'drivers';

    protected $fillable = [
        'user_id',
        'experience',
        'passport_front_picture',
        'passport_back_picture',
        'passport_expiration_date',
        'drivers_license_front_picture',
        'drivers_license_back_picture',
        'drivers_license_expiration_date',
    ];

    protected $guarded = [
        'id',
        'updated_at',
        'created_at',
        'confirmed_by',
        'confirmed_at',
    ];

    protected $hidden = [];
}
