<?php

namespace App\Models;

use Carbon\Carbon;

class Trip extends BaseModel
{
    protected $table = 'trips';
    protected $with = [];
    protected $dates = ['created_at', 'updated_at', 'datetime'];
    protected $appends = [];
    protected $casts = ['contacts' => 'array'];

    protected $fillable = [
        'from_id',
        'to_id',
        'persons',
        'price',
        'datetime',
        'note',
        'contacts',
    ];

    protected $guarded = [
        'id',
        'user_id',
        'updated_at',
        'created_at',
    ];

    protected $hidden = [
        'updated_at',
    ];

    public function user() {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
