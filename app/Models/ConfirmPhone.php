<?php

namespace App\Models;

class ConfirmPhone extends BaseModel
{
    protected $table = 'confirm-phones';

    protected $fillable = [];

    protected $guarded = [
        'id',
        'sms_id',
        'user_id',
        'encrypted_code',
        'phone_number',
        'attempts',
        'confirmed',
        'updated_at',
        'created_at'
    ];

    protected $hidden = [
        'encrypted_code'
    ];
}
