<?php

namespace App\Http\Controllers\V1;

use App\Models\Session;

class UserController extends Controller
{
    public function get_me()
    {
        return [$this->user];
    }

    public function update_me()
    {
        return [[]];
    }

    public function update_session()
    {
        /*
         * Response:
         *      -
         */

        $this->_validate([
            'version_code' => 'bail|required|integer',
            'fcm_token' => 'bail|string',
        ]);

        $params = $this->_getRequestParams([
            'version_code',
            'fcm_token',
        ]);

        $session = Session::where('user_id', $this->user->id)->first();

        // TODO: need to check version code (exists or not)

        $session->fcm_token = $params['fcm_token'];
        $session->version_code = $params['version_code'];

        Session::where('user_id', '!=', $this->user->id)->where('fcm_token', $params['fcm_token'])->delete();

        $session->save();

        return [[]];
    }
}
