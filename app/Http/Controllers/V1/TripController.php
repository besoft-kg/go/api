<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ResponseException;
use App\Models\Place;
use App\Models\Trip;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class TripController extends Controller
{
    public function create() {
        /*
         * Response:
         *      -from_place_not_found
         *      -to_place_not_found
         *      -invalid_datetime
         */

        $this->_validate([
            'from' => 'bail|required|integer',
            'to' => 'bail|required|integer',
            'price' => 'bail|integer|min:50|max:50000',
            'persons' => 'bail|required|integer|min:1|max:10',
            'datetime' => 'bail|required|numeric',
            'note' => 'bail|string|max:1000',
            'who' => 'bail|string|in:driver,passenger',
        ]);

        $params = $this->_getRequestParams([
            'from',
            'to',
            'persons',
            'price',
            'datetime',
            'note',
            'who',
        ]);

        if (!isset($params['who'])) $params['who'] = 'driver';

        if ($params['from'] == $params['to']) throw new ResponseException();
        $params['datetime'] = Carbon::createFromTimestamp($params['datetime']);
        if (Carbon::now() > $params['datetime'] || Carbon::now()->addWeek() < $params['datetime']) throw new ResponseException('', 'invalid_datetime');

        if (!Place::exists($params['from'])) throw new ResponseException('', 'from_place_not_found');
        if (!Place::exists($params['to'])) throw new ResponseException('', 'to_place_not_found');

        $phone_number = $this->user->phone_number;
        $note = $params['note'];

        if ($this->user->id === env('APP_DEBUG') ? 1 : 42 && substr($params['note'], 0, 3) === '~~~') {
            $exploded = explode('~~~', $params['note']);
            if (isset($exploded[1])) $phone_number = $exploded[1];
            if (isset($exploded[2])) $note = $exploded[2];
        }

        $trip = new Trip();
        $trip->user_id = $this->user->id;
        $trip->from_id = $params['from'];
        $trip->to_id = $params['to'];
        $trip->persons = $params['persons'];
        if ($params['price']) $trip->price = $params['price'];
        $trip->datetime = $params['datetime'];
        $trip->note = $note;
        $trip->contacts = [[
            'type' => 'phone_number',
            'value' => $phone_number,
            'data' => ['whatsapp' => false, 'telegram' => false],
        ]];
        $trip->who = $params['who'];

        $trip->save();
        $trip->refresh();

        return [$trip];
    }

    public function get() {
        /*
         * Response:
         *      -trip_not_found
         */

        $this->_validate([
            'from_id' => 'bail|integer',
            'to_id' => 'bail|integer',
            'user_id' => 'bail|integer',
            'who' => 'bail|string|in:driver,passenger',
        ], 'get', 'datetime');
        $params = $this->_getRequestParams([], 'get');
        $item = Trip::with('user')->where('datetime', '>', Carbon::now()->subHours(5));

        if ($params['id']) {
            $item->where('id', $params['id']);
            if (!$item->exists()) throw new ResponseException('', 'trip_not_found');
            return [$item->first()];
        }

        if (isset($params['from_id'])) $item->where('from_id', $params['from_id']);
        if (isset($params['to_id'])) $item->where('to_id', $params['to_id']);
        if (isset($params['user_id'])) $item->where('user_id', $params['user_id']);
        if (isset($params['who'])) $item->where('who', $params['who']);

        $count = $item->count();
        if ($params['_after_by'] && $params['_after']) $item->where($params['_after_by'], $params['_sorting'] === 'desc' ? '<' : '>', $params['_after']);

        return [[
            'data' => $item->orderBy($params['_order_by'], $params['_sorting'])
                ->limit($params['_limit'])
                ->get(),
            'pagination' => [
                'all' => $count,
            ],
        ]];
    }

    public function delete() {
        /*
         * Response:
         *      -trip_not_found
         */

        $this->_validate([
            'id' => 'bail|required|integer',
        ]);

        $params = $this->_getRequestParams([
            'id',
        ]);

        $item = Trip::where('id', $params['id'])->where('user_id', $this->user->id)->first();

        if (!$item) throw new ResponseException('', 'trip_not_found');

        if (!$item->delete()) throw new ResponseException();

        return [[]];
    }

    public function get_history() {
        /*
         * Response:
         *      -
         */

        $this->_validate([], 'get', 'datetime');
        $params = $this->_getRequestParams([], 'get');
        $item = Trip::where('user_id', $this->user->id)->where('datetime', '<=', Carbon::now()->subHours(5));

        $count = $item->count();
        if ($params['_after_by'] && $params['_after']) $item->where($params['_after_by'], $params['_sorting'] === 'desc' ? '<' : '>', $params['_after']);

        return [[
            'data' => $item->orderBy($params['_order_by'], $params['_sorting'])
                ->limit($params['_limit'])
                ->get(),
            'pagination' => [
                'all' => $count,
            ],
        ]];
    }

    public function webhook() {
        Log::debug($this->request);
        return [[]];
    }
}
