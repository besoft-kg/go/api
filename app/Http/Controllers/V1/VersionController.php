<?php

namespace App\Http\Controllers\V1;

use App\Models\Version;

class VersionController extends Controller
{
    public function get_last()
    {
        $this->_validate([
            'platform' => 'bail|required|string|in:android,ios,web',
        ]);

        return [Version::where('platform', $this->request->get('platform'))->orderBy('version_code', 'desc')->limit(1)->first()];
    }
}
