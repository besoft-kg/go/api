<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ResponseException;
use App\Models\ConfirmPhone;
use App\Models\Session;
use App\Models\User;
use App\Services\SmsService;
use App\Utils\Generator;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function checkPhone()
    {
        /*
         * Response:
         *      exists_code_was_sent
         *      exists_code_is_sent
         *      not_exists_code_was_sent
         *      not_exists_code_is_sent
         */

        $this->_validate([
            'phone_number' => 'bail|required|regex:/^[9][9][6][0-9]{9}$/'
        ]);

        $phone_number = $this->request->get('phone_number');

        $user = User::where('phone_number', $phone_number)->first();

        if (!$user) {
            return $this->_dbTransactionAndTryCatch(function () use ($phone_number) {
                if (ConfirmPhone::where('phone_number', $phone_number)->where('confirmed', false)->where('attempts', '>', 0)->first()) {
                    return [null, 'not_exists_code_was_sent'];
                } else {
                    $code = $phone_number === '996504290100' ? 934578 : Generator::verificationCode();

                    $sms = new SmsService(
                        $phone_number,
                        'Код подтверждения вашего номера: ' . $code,
                        'sign-up',
                        ['code' => $code],
                        $phone_number === '996504290100'
                    );

                    $sms = $sms->send();

                    $confirm_phone = new ConfirmPhone();
                    $confirm_phone->sms_id = $sms->id;
                    $confirm_phone->phone_number = $phone_number;
                    $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

                    $confirm_phone->save();

                    return [null, 'not_exists_code_is_sent'];
                }
            });
        } else {
            if (ConfirmPhone::where('user_id', $user->id)->where('confirmed', false)->where('attempts', '>', 0)->first()) {
                return [null, 'exists_code_was_sent'];
            } else {
                return $this->_dbTransactionAndTryCatch(function () use ($user) {
                    $code = $user->phone_number === '996504290100' ? 934578 : Generator::verificationCode();

                    $sms = new SmsService(
                        $user->phone_number,
                        'Код подтверждения вашего номера: ' . $code,
                        'sign-in',
                        ['code' => $code],
                        $user->phone_number === '996504290100'
                    );

                    $sms = $sms->send();

                    $confirm_phone = new ConfirmPhone();
                    $confirm_phone->sms_id = $sms->id;
                    $confirm_phone->user_id = $user->id;
                    $confirm_phone->phone_number = $user->phone_number;
                    $confirm_phone->encrypted_code = md5($code . env('APP_KEY'));

                    $confirm_phone->save();

                    return [null, 'exists_code_is_sent'];

                });
            }
        }
    }

    public function registerPhone()
    {
        /*
         * Response:
         *      -user_already_exists
         *      -code_is_invalid
         *      -code_is_incorrect
         */

        $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'code' => 'bail|required|integer|digits:6',
            'fcm_token' => 'bail|string',
            'language' => 'bail|required|string|in:ru,ky',
            'full_name' => 'bail|required|string|min:3|max:250',
            'gender' => 'bail|required|string|in:male,female',
            'platform' => 'bail|required|string|in:web,android,ios',
            'version_code' => 'bail|required|integer',
        ]);

        $params = $this->_getRequestParams([
            'phone_number',
            'code',
            'fcm_token',
            'language',
            'full_name',
            'gender',
            'platform',
            'version_code',
            'type',
        ]);

        $user = User::where('phone_number', $params['phone_number'])->first();

        if ($user) throw new ResponseException('', 'user_already_exists');

        $confirm_phone = ConfirmPhone::where('phone_number', $params['phone_number'])->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) throw new ResponseException('Код недействительный или у вас нет попыток!', 'code_is_invalid');

        return $this->_dbTransactionAndTryCatch(function () use ($confirm_phone, $params, $user) {
            if ($confirm_phone->encrypted_code === md5($params['code'] . env('APP_KEY'))) {
                $user = new User();
                $user->phone_number = $params['phone_number'];
                $user->full_name = $params['full_name'];
                $user->gender = $params['gender'];
                $user->language = $params['language'];

                $user->save();

                $confirm_phone->confirmed = true;
                $confirm_phone->user_id = $user->id;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $sessionKey = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->key = $sessionKey;
                $session->expired_at = $expired_at;
                $session->platform = $params['platform'];
                $session->version_code = $params['version_code'];
                $session->user_agent = $this->request->header('User-Agent');

                if ($this->request->has('fcm_token') && $params['fcm_token']) {
                    $session->fcm_token = $params['fcm_token'];
                    Session::where('user_id', '!=', $user->id)->where('fcm_token', $params['fcm_token'])->delete();
                }

                $session->save();

                return [[
                    'token' => Generator::jwt($user, $sessionKey, $expired_at->timestamp),
                    'user' => $user
                ]];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                throw new ResponseException(['attempts' => $confirm_phone->attempts], 'code_is_incorrect');
            }
        });
    }

    public function loginPhone()
    {
        /*
         * Response:
         *      -code_is_incorrect
         *      -code_is_invalid
         *      -user_not_found
         */

        $this->_validate([
            'phone_number' => 'bail|required|integer|regex:/^[9][9][6][0-9]{9}$/',
            'code' => 'bail|required|integer|digits:6',
            'fcm_token' => 'bail|string',
            'language' => 'bail|required|string|in:ru,ky',
            'platform' => 'bail|required|string|in:web,android,ios',
            'version_code' => 'bail|required|integer',
        ]);

        $phone_number = $this->request->get('phone_number');
        $code = $this->request->get('code');

        $user = User::where('phone_number', $phone_number)->first();

        if (!$user) throw new ResponseException('', 'user_not_found');

        $confirm_phone = ConfirmPhone::where('user_id', $user->id)->where('confirmed', false)->where('attempts', '>', 0)->orderBy('created_at', 'desc')->first();

        if (!$confirm_phone) throw new ResponseException('', 'code_is_invalid');

        return $this->_dbTransactionAndTryCatch(function () use ($confirm_phone, $code, $user) {
            if ($confirm_phone->encrypted_code === md5($code . env('APP_KEY'))) {
                $confirm_phone->confirmed = true;
                $confirm_phone->save();

                $expired_at = Carbon::now()->addMonth();
                $sessionKey = Generator::sessionKey();

                $session = new Session();
                $session->user_id = $user->id;
                $session->key = $sessionKey;
                $session->expired_at = $expired_at;
                $session->version_code = $this->request->get('version_code');
                $session->platform = $this->request->get('platform');
                $session->user_agent = $this->request->header('User-Agent');

                if ($this->request->has('fcm_token')) {
                    $session->fcm_token = $this->request->get('fcm_token');
                    Session::where('user_id', '!=', $user->id)->where('fcm_token', $this->request->get('fcm_token'))->delete();
                }

                $session->save();

                $user->language = $this->request->get('language');

                if ($user->isDirty()) $user->save();

                return [[
                    'token' => Generator::jwt($user, $sessionKey, $expired_at->timestamp),
                    'user' => $user
                ]];
            } else {
                $confirm_phone->attempts--;
                $confirm_phone->save();
                throw new ResponseException(['attempts' => $confirm_phone->attempts], 'code_is_incorrect');
            }
        });
    }
}
