<?php

namespace App\Http\Controllers\V1;

use App\Exceptions\ResponseException;
use App\Models\Vehicle;

class VehicleController extends Controller
{
    public function create() {
        /*
         * Response:
         *      -already_exists
         */

        $this->_validate([
            'title' => 'bail|required|string|min:3|max:50',
            'type' => 'bail|required|string|in:minivan,passenger',
            'data_sheet_back_picture' => 'bail|image',
            'data_sheet_front_picture' => 'bail|image',
            'picture' => 'bail|image',
        ]);

        // TODO: need to save images

        $params = $this->_getRequestParams([
            'title', 'type',
            'data_sheet_back_picture',
            'data_sheet_front_picture',
            'front_picture',
            'back_picture'
        ]);

        if (Vehicle::where('title', $params['title'])->where('user_id', $this->user->id)->exists()) throw new ResponseException('', 'already_exists');

        $vehicle = new Vehicle();
        $vehicle->user_id = $this->user->id;
        $vehicle->title = $params['title'];
        $vehicle->type = $params['type'];

        $vehicle->save();

        return [$vehicle];
    }

    public function get() {
        /*
         * Response:
         *      -vehicle_not_found
         */

        $this->_validate([], 'get', 'title');
        $params = $this->_getRequestParams([], 'get');
        $item = Vehicle::where('user_id', $this->user->id);

        if ($params['id']) {
            $item = $item->where('id', $params['id']);
            if (!$item->exists()) throw new ResponseException('', 'vehicle_not_found');
            return [$item->first()];
        }

        $count = $item->count();
        if ($params['_after_by'] && $params['_after']) $item->where($params['_after_by'], $params['_sorting'] === 'desc' ? '<' : '>', $params['_after']);

        return [[
            'data' => $item->orderBy($params['_order_by'], $params['_sorting'])->limit($params['_limit'])->get(),
            'pagination' => [
                'all' => $count,
            ],
        ]];
    }
}
