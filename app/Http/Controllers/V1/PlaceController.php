<?php

namespace App\Http\Controllers\V1;

use App\Models\Place;

class PlaceController extends Controller
{
    public function get() {
        return [Place::all()];
    }
}
