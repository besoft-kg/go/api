<?php

namespace App\Utils;

use App\Models\Session;
use Carbon\Carbon;
use Firebase\JWT\JWT;

class Generator
{
    static function jwt($user, $sub, $exp)
    {
        $payload = [
            'iss' => "besoft-go",
            'sub' => ['user' => $user->id, 'key' => $sub],
            'iat' => Carbon::now()->timestamp,
            'exp' => $exp
        ];

        return JWT::encode($payload, env('JWT_SECRET'));
    }

    static function verificationCode() {
        if (env('APP_DEBUG')) return 123456;
        return rand(100000, 999999);
    }

    static function randomString($length = 32) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    static function sessionKey() {
        $key = Generator::randomString(64);

        if (Session::where('key', $key)->exists()) {
            return Generator::sessionKey();
        }

        return $key;
    }
}
