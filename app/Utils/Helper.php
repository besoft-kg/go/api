<?php

namespace App\Utils;

use App\Models\Notification;
use Illuminate\Support\Facades\Auth;

class Helper
{
    public static function sendPushNotification($ids, $notification = null, $data = [])
    {
        $fields = [
            'data' => array_merge($data, [
                '_unread_notifications_count' => Notification::where('user_id', Auth::user()->id)->whereNull('read_on')->count(),
            ]),
            'android' => [
                'default_vibrate_timings' => true,
                'default_light_settings' => true,
                'default_sound' => true,
            ],
            'priority' => 'high',
        ];

        if (count($ids) === 1) $fields['to'] = $ids[0];
        else $fields['registration_ids'] = $ids;

        if ($notification) {
            $fields['notification'] = array_merge($notification, [
                'sound' => 'default',
                'priority' => 'high',
            ]);
        }

        $API_ACCESS_KEY = 'AAAAg9fv8Eg:APA91bH3AGoz9xLnyqPv_UBOQEtW9yoLyShzC-HyglFI9zkIMl6qo708xN_C3A-z5yIuY6TnDdw38Dl-lCH5fivHAn-TKcCrGQx6ghUUkAgwqau2NrDfM3--72VQGZ4i8GY2Gl5bbNG8';
        $headers = [
            'Authorization: Bearer ' . $API_ACCESS_KEY,
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }
}
