<?php

namespace App\Exceptions;

use Exception;

class ResponseException extends Exception
{
    public function __construct($message = "", $code = 'unknown_error')
    {
        $this->message = $message;
        $this->code = $code;
    }
}
