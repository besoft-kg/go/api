<?php

namespace App\Providers;

use App\Exceptions\ResponseException;
use App\Models\Session;
use App\Models\User;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Exception;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            $header = $request->header('Authorization', '');
            $token = null;

            if (Str::startsWith($header, 'Bearer ')) {
                $token = Str::substr($header, 7);
            }

            if ($token) {
                try {
                    $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                } catch(ExpiredException $e) {
                    throw new ResponseException('', 'token_is_expired');
                } catch(Exception $e) {
                    throw new ResponseException('', 'token_is_invalid');
                }

                $session = Session::where('key', $credentials->sub->key)->where('user_id', $credentials->sub->user)->first();

                if (!$session) throw new ResponseException('', 'token_is_invalid');
                else Session::where('user_id', $credentials->sub->user)->where('platform', $session->platform)->where('id', '!=', $session->id)->delete();

                return User::where('id', $session->user_id)->first();
            } else {
                return null;
            }
        });
    }
}
