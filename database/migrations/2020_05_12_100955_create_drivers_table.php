<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->integer('experience')->nullable();

            $table->unsignedBigInteger('confirmed_by')->nullable();
            $table->datetime('confirmed_at')->nullable();

            $table->string('passport_front_picture', 255)->nullable();
            $table->string('passport_back_picture', 255)->nullable();
            $table->date('passport_expiration_date')->nullable();

            $table->string('drivers_license_front_picture', 255)->nullable();
            $table->string('drivers_license_back_picture', 255)->nullable();
            $table->date('drivers_license_expiration_date')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('confirmed_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
