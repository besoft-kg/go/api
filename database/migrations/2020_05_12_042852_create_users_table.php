<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('phone_number', 55)->unique();
            $table->string('full_name', 255);
            $table->enum('gender', ['male', 'female']);
            $table->json('contacts')->nullable();
            $table->json('abilities')->nullable();
            $table->string('picture', 255)->nullable();
            $table->longText('bio')->nullable();
            $table->enum('language', ['ky', 'ru'])->default('ru');
            $table->timestamp('last_action')->useCurrent();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
