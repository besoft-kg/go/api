<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->id();

            $table->string('title', 55);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('confirmed_by')->nullable();
            $table->datetime('confirmed_at')->nullable();
            $table->string('state_number', 55)->nullable();
            $table->string('picture', 255)->nullable();
            $table->string('data_sheet_front_picture', 255)->nullable();
            $table->string('data_sheet_back_picture', 255)->nullable();
            $table->date('data_sheet_expiration_date')->nullable();
            $table->smallInteger('persons')->nullable();
            $table->enum('type', ['passenger', 'minivan']);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('confirmed_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
