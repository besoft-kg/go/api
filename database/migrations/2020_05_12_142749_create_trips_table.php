<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('from_id');
            $table->unsignedBigInteger('to_id');
            $table->smallInteger('persons');
            $table->integer('price')->nullable();
            $table->timestamp('datetime');
            $table->text('note')->nullable();
            $table->json('contacts')->nullable();
            $table->enum('who', ['driver', 'passenger'])->default('driver');

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('from_id')->references('id')->on('places');
            $table->foreign('to_id')->references('id')->on('places');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
