<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('added_by');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('title_ky', 55);
            $table->string('title_ru', 55);
            $table->integer('position')->nullable();

            $table->foreign('added_by')->references('id')->on('users');
            $table->foreign('parent_id')->references('id')->on('places');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
