<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group([
    'namespace' => 'V1',
    'prefix' => 'v1'
], function () use ($router) {
    $router->group(['middleware' => ['auth']], function () use ($router) {

        // AUTH REQUIRED

        $router->get('/me', 'UserController@get_me');
        $router->put('/me', 'UserController@update_me');

        //$router->get('/me/session', 'UserController@get_session');
        $router->put('/me/session', 'UserController@update_session');

        $router->post('/trip', 'TripController@create');
        $router->get('/trip', 'TripController@get');
        $router->delete('/trip', 'TripController@delete');
        //$router->put('/trip', 'TripController@update');

        $router->get('/trip/history', 'TripController@get_history');

        //$router->post('/vehicle', 'VehicleController@create');
        //$router->get('/vehicle', 'VehicleController@get');
        //$router->put('/vehicle', 'VehicleController@update');

    });

    $router->group([], function () use ($router) {

        // AUTH

        $router->post('/auth/phone', 'AuthController@checkPhone');
        $router->post('/auth/phone/login', 'AuthController@loginPhone');
        $router->post('/auth/phone/register', 'AuthController@registerPhone');

        $router->get('/place', 'PlaceController@get');

        $router->get('/version/last', 'VersionController@get_last');

        $router->post('/webhook', 'TripController@webhook');

    });
});

$router->get('/', function () {
    return response(['Besoft Go API is running...']);
});
